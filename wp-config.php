<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'LC_Bardo_db');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '/5|TX(<`A29RjVfSY*k], j*5NykmNzUS4/_oyTbp?%Lx*g*]XX+njCm&1T:Zhk4');
define('SECURE_AUTH_KEY',  'c3wpR<+siU}fna|N6@wu8oWB8|`{r&gjVsB nbpu(dUY@c{%7aOSEm{oet:)y67P');
define('LOGGED_IN_KEY',    '0Hx@{8XS[6x6H82:|8cYh&LC=&i#y;]IW<c.(t3rEVKp$c>nvL)3q x}B=fK5BY>');
define('NONCE_KEY',        '!Vy+YPZUHT92/Bgkah+Sc$~<2rM JJOj&@7lFe+:$(4rG`;}2C<iNVc|~P9(tKfW');
define('AUTH_SALT',        'd{&|0@{$c?6O^SfCl/sD)n_]f@r>-M|yRzGY{aV:+!TqQJ&u;6b_fZjl_{e`1H[U');
define('SECURE_AUTH_SALT', 'k6aB+dd2({$39{ cxQ*+DT)OAx>-3q>!;JIxnj(r+f722@UswL&O*{RP}j>^WA&r');
define('LOGGED_IN_SALT',   '[|k7U8Y#hxkh-Uu#)^4K@^y6twl7TGPod:55>rp5>Aq_Y(R1vY!b$)&niKI_tF  ');
define('NONCE_SALT',       'h<i$G~gmmI7=Q=!J89?lv/vZano?eo|G~7u&(2{K3Y9AoE(F|n_/+wBQA+.5`K=a');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'lcb_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
